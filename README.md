## Robot ##

Al crearlo se han tenido en cuenta las siguientes premisas:
- Ocultar todo cableado y dejarlo ordenado, de forma similar a una placa de circuíto impreso
- Evitar el cambio de baterías
- Crear un chasis muy estable
- No seguir algoritmos, intentar crear un sistema básico de Inteligencia Artificial (responde a lo que vé, ni memoriza nada ni cuenta pasos)
- Mantener un coste mínimo

A día de hoy se ha conseguido...
- Lo de hacer laberintos es lo que peor lleva :sweat_smile:, pero ha estado estudiando y ya es capaz de ir recto entre dos paredes sin intentar derribarlas, gira bien en las curvas de 90º...
- El chasis es muy estable pudiendo acelerar a fondo y hacer giros bruscos sin derrapar (no se desvía en línea recta :ok_hand:)
- Funciona como coche radiocontrol por Bluetooth, se le añade una pequeña mochila (modulo bluetooth SPP-CA) y a jugar con él desde el portátil o el móvil
- Le planta cara a los minisumo (controlándolo por bluetooth o cargándole el software de uno de sus hermanos no tarda mucho en ponerse a empujar con todo lo que tiene)
- El coste total del robot rondará los 10€ y, salvo el programador, incluye todo lo necesario (hasta lleva cargador de baterías! :speak_no_evil:)

## Historia ##

# 2016 Primer año: Está como una cabra...

Fué lo que pensé sobre el robot minutos antes de presentarlo el primer año, así que cuando me preguntaron el nombre lo tenía claro: CabraTola!
Aquel año usaba sensores de luz LDR, usaba un láser para decirle que arrancase (apuntando al LDR frontal para que recibiese un valor muy alto) y se calibraba girando sobre sí mismo a toda velocidad (durante un rato leía máximo, mínimo y al acabar de girar calculaba la mitad)
La falta de experiencia, la mala suerte... todos jugaron en contra y no llegó más que a darse de ostias con las paredes de la primera casilla haciendo honor a su nombre; al encarar la salida, el LDR frontal se encontraba con la luz del proyector y se daba la vuelta pensando que había encontrado una pared, el código (aunque "funcionaba" en laberintos pequeños) era demasiado caótico, y tener un regulador de  voltaje casi quemado lo dejaba miope perdido (todavía tiene la madera quemada cerca del arduino)


# 2017 Segundo año: No lo presento!

Sabía que iba a hacer el ridículo, lo había probado antes en el minilaberinto y lo hacía todo mal, ni se parecía a como se portaba en casa y no me daba tiempo a arreglarlo... pero lo presenté, llevaba unos nuevos sensores de infrarrojos, le había quitado el regulador quemado a favor del que viene integrado en el arduino y reescrito el código para... que llegase a la primera pared, no llegó más lejos


# 2018 Tercer año: Futurama!, la nave (cuadrada) de Planet Express impresa en 3D :blush:

No era CabraTola, ni siquiera era mi robot así que... qué hacía yo allí presentando otro robot que no iba a funcionar en vez de aquel en el que había estado trabajando en verano? (que tampoco funcionaría)
Fácil, Sandía Express es el hermano "gemelo" de CabraTola y, de hecho, llevaba su software (por eso no funcionó :smirk: :point_right: :point_right:)
Sandía era el robot de laberinto en el que estaba trabajando uno de mis compañeros de piso, el culpable de que me dedique a hacer robots, Yago :eyes:. El problema de Sandía es que llevaba las ruedas en el medio con apoyos delante y atrás (cosa que lo hacía inestable), si además le sumábamos los problemas con el cableado y la complejidad de hacer un software para estos robots... el pobre estaba abandonado en una estantería.
Como siempre me decía que le encantaba como diseño los robots y de aquellas estaba aprendiendo a imprimir en 3D se lo pedí un fin de semana y lo reconstruí de arriba a abajo, le cargué el software de CabraTola para probarlo y así nació Sandía Express, por lo menos aprendí que los sensores de ultrasonidos HC-SR04 no son una buena idea para laberintos :sweat_smile:
(Sigo insistiéndole para que le haga el software, pero no hay manera :disappointed_relieved:)


# 2019 Cuarto año: Me da que no... :sweat_smile: