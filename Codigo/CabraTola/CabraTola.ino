// Alberto Ferreiro Campello "Kampy"
// Project Maze-MicroMouse
// 3 LDR
// http://oshwdem.org



////////////////////////////////// Optimization & libraries ///////////////////////////////



// Optimization flags, if any library are used, place this line first
  #pragma GCC optimize ("-O3")



/////////////////////////////// Variable & Constant declaration /////////////////////////////



// Internal led
  #define LED 13
  
// Motor pins
// Increase Front and decrease Back to go fordward
// Decrease Front and increase Back to go backwards
// Same value in front and back stops the motor
  #define Back_Left 10
  #define Front_Left 5
  #define Back_Right 6
  #define Front_Right 9

// LDR pins
  #define left A0 //A6 A5 A2 A1
  #define right A4
  #define under A7
  #define front A3

// Bluetooth Key
  int BKey = 5;
  


/////////////////////////////////////// Initialize //////////////////////////////////////////



void setup()
{  
  ////// Pin Initialization //////
    pinMode(LED, OUTPUT);
    pinMode(Back_Right, OUTPUT);
    pinMode(Back_Left, OUTPUT);
    pinMode(Front_Left, OUTPUT);
    pinMode(Front_Right, OUTPUT);

  // Start with the motors off
    bot_stop();

  // Serial initialization
    Serial.begin(9600);
    digitalWrite(LED, HIGH);
}



//////////////////////////////////// Main body //////////////////////////////////////////////



void loop()
{
  if(Serial.available()>0)
  {
   BKey = Serial.read();
  }

  if(BKey == '5')
  {
    bot_stop();
  }
  else if(BKey == '4')
  {
    bot_left();
  }
  else if(BKey == '6')
  {
    bot_right();
  }
  else if(BKey == '2')
  {
    bot_back();
  }
  else if(BKey == '7')
  {
    bot_spin_left();
  }
  else if(BKey == '9')
  {
    bot_spin_right();
  }
  else if(BKey == '8')
  {
    bot_foward();
  }
  else
  {
    bot_stop();
  }
}



/////////////////////////////////// Auxiliary functions /////////////////////////////////////



// Set of functions to set the speed for each motor
// Inputs: -none-
// Outputs: Choosen movement of the bot
// Note: [100% = 255] - [75% = 190] - [50% = 127] - [25% = 63]
void bot_foward()
{
    analogWrite (Front_Right, 255);
    analogWrite (Front_Left, 255);
    analogWrite (Back_Right, 0);
    analogWrite (Back_Left, 0);
}
void bot_back()
{
    analogWrite (Front_Right, 0);
    analogWrite (Front_Left, 0);
    analogWrite (Back_Right, 255);
    analogWrite (Back_Left, 255);
}
void bot_left()
{
    analogWrite (Front_Right, 255);
    analogWrite (Front_Left, 0);
    analogWrite (Back_Right, 0);
    analogWrite (Back_Left, 0);
}
void bot_right()
{
    analogWrite (Front_Right, 0);
    analogWrite (Front_Left, 255);
    analogWrite (Back_Right, 0);
    analogWrite (Back_Left, 0);
}
void bot_spin_left()
{
    analogWrite (Front_Right, 255);
    analogWrite (Front_Left, 0);
    analogWrite (Back_Right, 0);
    analogWrite (Back_Left, 255);
}
void bot_spin_right()
{
    analogWrite (Front_Right, 0);
    analogWrite (Front_Left, 255);
    analogWrite (Back_Right, 255);
    analogWrite (Back_Left, 0);
}
void bot_stop()
{
    analogWrite (Front_Right, 0);
    analogWrite (Front_Left, 0);
    analogWrite (Back_Right, 0);
    analogWrite (Back_Left, 0);
}
